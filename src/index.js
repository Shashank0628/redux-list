import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import './index.css'
import {Provider} from 'react-redux';
import store from './redux/store';
import {BrowserRouter as Router} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';


ReactDOM.render(
    <Router>
        <Provider store = {store}>
            <App />
        </Provider>
    </Router>
 ,
    document.getElementById('root')
)