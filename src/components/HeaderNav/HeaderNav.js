import React from 'react'
import style from './HeaderNav.module.css';
import { Link } from 'react-router-dom';
const HeaderNav = () => {
    return (
        <nav className = {style.navbar}>
            <ul>
                <li><Link to = '/' className={style.navLinks}>Home</Link></li>
                <li><Link to = '/courses' className={style.navLinks}>Courses</Link></li>
                <li><Link to = '/About' className={style.navLinks}>About</Link></li>
            </ul>
        </nav>
    )
}

export default HeaderNav
