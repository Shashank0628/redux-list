export const SET_COURSES = 'SET_COURSES'

export const ADD_COURSE = 'ADD_COURSE'

export const UPDATE_COURSE = 'UPDATE_COURSE'

export const REMOVE_COURSE = 'REMOVE_COURSE'