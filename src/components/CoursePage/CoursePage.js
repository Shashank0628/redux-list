import React from 'react'
import { useSelector } from 'react-redux'
import CourseList from './CourseList/CourseList';
import {Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

const CoursePage = () => {
    const courses = useSelector(state => state.courses)

    return (
        <div>
            <div>
                <h3>Here are {courses.length} Courses</h3>
                <Button variant = 'primary'><Link to = '/courses/new' style = {{color: "white", textDecoration: "none"}}>New</Link></Button>
            </div>

            <br></br>
            <hr></hr>
            
            <CourseList courses = {courses}/>
        </div>
    )
}

export default CoursePage
