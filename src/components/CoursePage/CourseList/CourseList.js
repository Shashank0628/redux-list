import React  , {useEffect} from 'react'
import CourseCard from './CourseCard/CourseCard';

const CourseList = ({courses}) => {
  
    const emptyMessage = (<h2>There are no courses to fetch</h2>)

    const coursesList  = (
                            <div>
                                 {
                                    courses.map(course => <CourseCard course = {course}/>)
                                 }
                            </div>
                        )

    

    return (
        <div>
            {courses.length ? coursesList : emptyMessage}
        </div>
    )
}

export default CourseList
