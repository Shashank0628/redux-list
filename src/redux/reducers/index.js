import courseReducer from './courseReducer';
import {combineReducers} from 'redux';

const rootReducer = combineReducers({
    courses: courseReducer
})

export default rootReducer