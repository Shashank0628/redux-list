import {
  SET_COURSES,
  ADD_COURSE,
  UPDATE_COURSE,
  REMOVE_COURSE,
} from "../constants";

export const setCourses = (data) => {
  return {
    type: SET_COURSES,
    payload: data,
  };
};

export const addCourse = (data) => {
  return {
    type: ADD_COURSE,
    payload: data,
  };
};

export const updateCourse = (data) => {
  return {
    type: UPDATE_COURSE,
    payload: data,
  };
};

export const removeCourse = (id) => {
  return {
    type: REMOVE_COURSE,
    payload: id,
  };
};

export const postCourses = (data) => {
  return async (dispatch) => {
    const response = await fetch(
      `${process.env.REACT_APP_SERVER_URL}/api/courses`,
      {
        method: "POST",
        mode: "cors",
        cache: "no-cache",
        credentials: "same-origin",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      }
    );
    if (response.ok) {
      const data = await response.json();
      dispatch(addCourse(data.course));
      return data;
    } else {
      const err = new Error(response.statusText);
      err.response = response;
      throw err;
    }
  };
};

export const putCourse = (data) => {
  return async (dispatch) => {
    const response = await fetch(
      `${process.env.REACT_APP_SERVER_URL}/api/courses/${data._id}`,
      {
        method: "PUT",
        mode: "cors",
        cache: "no-cache",
        credentials: "same-origin",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      }
    );
    if (response.ok) {
      const data = await response.json();
      dispatch(updateCourse(data.course));
      return data;
    } else {
      const err = new Error(response.statusText);
      err.response = response;
      throw err;
    }
  };
};

export const deleteGame = (id) => {
  return async (dispatch) => {
    const response = await fetch(
      `${process.env.REACT_APP_SERVER_URL}/api/courses/${id}`,
      {
        method: "DELETE",
        mode: "cors",
        cache: "no-cache",
        credentials: "same-origin",
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    if (response.ok) {
      const data = await response.json();
      dispatch(removeCourse(id));
      return data;
    } else {
      const err = new Error(response.statusText);
      err.response = response;
      throw err;
    }
  };
};

export const fetchCourses = () => {
  return async (dispatch) => {
    const response = await fetch(
      `${process.env.REACT_APP_SERVER_URL}/api/courses`
    );
    const data = await response.json();
    dispatch(setCourses(data.courses));
  };
};
