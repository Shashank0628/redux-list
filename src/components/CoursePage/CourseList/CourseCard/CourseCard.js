import React from 'react';
import {Card , Button} from 'react-bootstrap'
import {Link} from 'react-router-dom';
import {useDispatch} from 'react-redux';
import {deleteGame} from '../../../../redux/actions/actions';

const CourseCard = ({course}) => {

    const dispatch = useDispatch()

    const clickHandler = () => {
        try{
            dispatch(deleteGame(course._id))
        }catch(err){
            console.log("Error")
        }
        
    }

    return (
        <Card style={{ width: '60%' }}>
            <Card.Body>
                        <Card.Title>{course.title}</Card.Title>

                        <Card.Text>
                            {course.authorId}
                        </Card.Text>

                        <Card.Text>
                            {course.category}
                        </Card.Text>

                        <Card.Text>
                            {course.length}
                        </Card.Text>

                        <Link to = {`/course/${course._id}`} className = 'btn btn-success btn-md'>Edit</Link>
                        <button className = 'btn btn-danger btn-md' style = {{marginLeft: "10px"}} onClick = {clickHandler}>Delete</button>
                        
            </Card.Body>
      </Card>
    )
}

export default CourseCard
