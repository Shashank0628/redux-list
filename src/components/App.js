import React , {useEffect} from 'react'
import {useSelector , useDispatch} from 'react-redux';
import { Switch , Route} from 'react-router';
import CoursePage from './CoursePage/CoursePage';
import CourseForm from './CourseForm/CourseForm';
import HeaderNav from './HeaderNav/HeaderNav';
import {fetchCourses} from '../redux/actions/actions'

const App = () => {
    const courses = useSelector(state => state.courses)
    const dispatch = useDispatch()

    useEffect(()=> {    
        dispatch(fetchCourses())    
    } , [])

    return (
        <div>
            <HeaderNav /> 
            <Switch>
                <Route exact path = '/courses' component = {CoursePage} />
                <Route exact path = '/courses/new' component = {CourseForm} />
                <Route exact path = '/course/:_id' component = {CourseForm} />
            </Switch>
        </div>
    )
}

export default App
