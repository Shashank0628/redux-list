import React  , {useState , useEffect} from 'react';
import {Form , Alert , Button , Card} from 'react-bootstrap';
import {useSelector , useDispatch} from 'react-redux';
import {postCourses , putCourse} from '../../redux/actions/actions';
import {useHistory} from 'react-router-dom';



const CourseForm = ({match}) => {
    const [formInput , setInput]  = useState({ _id: "" , title: "" , author: "" , category: "" , length: ""})
    const [error , setError] = useState("")
    const [loading , setLoading] = useState(false)

    const courses = useSelector(state => state.courses)
    const history = useHistory()
    const dispatch = useDispatch()

    
    const handleChange = (e) => {
        setInput((prev) => {
            return {...prev, [e.target.name] : e.target.value}
        })
    }

    const handleSubmit = async (e) =>{
    
        e.preventDefault()

        setLoading(true)

        try{
            if(formInput._id){
                const data = await dispatch(putCourse(formInput))
            }else{
                const data = await dispatch(postCourses(formInput))
            }
            history.push('/courses')
        }

        catch(err){
            setError(err.message)
            setInput({title: "" , author: "" , category: "" , length: ""})
            setLoading(false)
        }

    }

    
    useEffect(() => {
        if(match.params._id){
            const course = courses.find(course => course._id === match.params._id)
            if(course){
                    setInput({ _id: course._id , title: course.title , author: course.authorId , category: course.category , length: course.length})
            }
        }
    } , [courses])

    return (
         <div className='w-100' style = {{maxWidth: "1200px" , margin: "20px auto"}} >
           
            <Card>

                <Card.Body>

                    <h3 className='text-center mt-10 mb-10' >This is Create Page</h3>

                    {error && <Alert variant = "danger">{error}</Alert>}

                    <Form onSubmit = {handleSubmit}>

                        <Form.Group className = 'mb-3' >
                            <Form.Label>
                                Title
                            </Form.Label>
                            <Form.Control name = 'title' value = {formInput.title} type="text" placeholder="Enter the title"  onChange = {handleChange} required/>
                        </Form.Group>

                        <Form.Group className = 'mb-3' >
                            <Form.Label>
                                Author
                            </Form.Label>
                            <Form.Control as="select" name = 'author' value = {formInput.author} onChange = {handleChange} required>
                                <option value=""></option>
                                <option value = 'cory-house'>Cory House</option>
                                <option value = 'scott-allen'>Scott Allen</option>
                                <option value = 'dan-wahlin'>Dan Wahlin</option>
                            </Form.Control>
                        </Form.Group>


                        <Form.Group className = 'mb-3' >
                            <Form.Label>Category</Form.Label>
                            <Form.Control type = 'text' name = 'category' value = {formInput.category} onChange = {handleChange} required/>
                        </Form.Group>

                        <Form.Group className = 'mb-3' >
                            <Form.Label>Length</Form.Label>
                            <Form.Control type = 'text' name = 'length' value = {formInput.length} onChange = {handleChange} required/>
                        </Form.Group>

                        <Button type='submit' className="w-100 mt-2" disabled = {loading}>
                            Submit
                        </Button>

                    </Form>

                </Card.Body>

            </Card>

        </div> 
        
    )
}

export default CourseForm
