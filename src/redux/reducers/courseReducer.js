import initialState from '../initialState';
import {SET_COURSES , ADD_COURSE , UPDATE_COURSE , REMOVE_COURSE} from '../constants';

export default (state = initialState.course , action) => {
    switch(action.type){
        case SET_COURSES:
            return action.payload
            
        case ADD_COURSE:
            return [...state , action.payload]
        
        case UPDATE_COURSE:
            return (
                state.map(course => {
                    if(course._id === action.payload._id){
                        return action.payload
                    }
                    return course
                })
            )

        case REMOVE_COURSE:
            return (
                state.filter(course => course._id !== action.payload)
            )

        default: 
            return state
    }
}